package com.example.aritetrisburg;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class EnterNameScreen extends AppCompatActivity {
    Button toPlayButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enter_name_screen);

        toPlayButton = findViewById(R.id.btnName);
        toPlayButton.setOnClickListener(v -> {
            Intent intent = new Intent(EnterNameScreen.this, MainActivity.class);
            startActivity(intent);
        });
    }
}

