package com.example.aritetrisburg;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, GestureDetector.OnGestureListener {

    DrawView drawView;
    GameState gameState;
    RelativeLayout gameButtons;
    Button left;
    Button right;
    Button rotateAc;
    FrameLayout game;
    Button pause;
    TextView score;
    Button difficultyToggle;
    //Button restart;
    Handler handler;
    Runnable loop;

    int delayFactor;
    int delay;
    int scoreCounter;
    int delayLowerLimit;
    private static final String TAG = "Swipe Position";
    private float x1, x2, y1, y2;
    private static int MIN_DISTANCE = 400;
    private GestureDetector gestureDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.song2);
        mediaPlayer.start();
        mediaPlayer.setLooping(true);
        super.onCreate(savedInstanceState);
        this.gestureDetector = new GestureDetector(MainActivity.this, this);

        gameState = new GameState(24, 20, TetraminoType.getRandomTetramino());

        drawView = new DrawView(this, gameState);
        drawView.setBackgroundResource(R.drawable.background);
        //drawView.setBackgroundColor(Color.BLACK);

        game = new FrameLayout(this);
        gameButtons = new RelativeLayout(this);

        delay = 250;
        delayLowerLimit = 200;
        delayFactor = 2;

        pause = new Button(this);
        pause.setText(R.string.pause);
        pause.setId(R.id.pause);

        score = new TextView(this);
        score.setText(R.string.score);
        score.setId(R.id.score);
        score.setTextSize(30);

        difficultyToggle = new Button(this);
        difficultyToggle.setText(R.string.easy);
        difficultyToggle.setId(R.id.difficulty);

//        restart = new Button(this);
//        restart.setText(R.string.restart);
//        restart.setId(R.id.restart);

        RelativeLayout.LayoutParams rl = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);RelativeLayout.LayoutParams leftButton = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        RelativeLayout.LayoutParams pausebutton = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        RelativeLayout.LayoutParams scoretext = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        RelativeLayout.LayoutParams speedbutton = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        //RelativeLayout.LayoutParams restartbutton = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        gameButtons.setLayoutParams(rl);
        gameButtons.addView(pause);
        gameButtons.addView(score);
        gameButtons.addView(difficultyToggle);
        //gameButtons.addView(restart);

        pausebutton.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        pausebutton.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);

        scoretext.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        scoretext.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);

        speedbutton.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        speedbutton.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);

        //restartbutton.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        //restartbutton.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);

        pause.setLayoutParams(pausebutton);
        score.setLayoutParams(scoretext);
        difficultyToggle.setLayoutParams(speedbutton);
        //restart.setLayoutParams(restartbutton);

        game.addView(drawView);
        game.addView(gameButtons);
        setContentView(game);

        View pauseButtonListener = findViewById(R.id.pause);
        pauseButtonListener.setOnClickListener(this);

        View speedButtonListener = findViewById(R.id.difficulty);
        speedButtonListener.setOnClickListener(this);
//__________________________________________________________________________________________________
//        View restartButtonListener = findViewById(R.id.restart);
//        restartButtonListener.setOnClickListener(this);

        handler = new Handler(Looper.getMainLooper());
        loop = new Runnable() {
            public void run() {
                if (gameState.status) {
                    if (!gameState.pause) {
                        boolean success = gameState.moveFallingTetraminoDown();
                        if (!success) {
                            gameState.paintTetramino(gameState.falling);
                            gameState.lineRemove();

                            gameState.pushNewTetramino(TetraminoType.getRandomTetramino());

                            if (gameState.score % 10 == 9 && delay >= delayLowerLimit) {
                                delay = delay / delayFactor + 1;
                            }

                            scoreCounter = gameState.incrementScore();

//                            /_____________________________________________________________________
//                            Intent intent1 = new Intent(MainActivity.this, ScoreLeaderboardScreen.class);
//                            intent1.putExtra("points", score);
             //               MainActivity.this.startActivity(intent1);
           //                 ((Activity) MainActivity.this).finish();
                        }
                        drawView.invalidate();
                        handler.postDelayed(this, delay);
                    }
                    else {
                        handler.postDelayed(this, delay);
                    }
            }
                else {
                    //pause.setText(R.string.start_new_game);

                    Intent intent2 = new Intent(MainActivity.this, GameOverScreen.class);
                    startActivity(intent2);
                    mediaPlayer.stop();
                    finish();
                }
            }

        };
        loop.run();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        gestureDetector.onTouchEvent(event);
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                y1 = event.getY();
                break;
            case MotionEvent.ACTION_UP:
                x2 = event.getX();
                y2 = event.getY();

                float valueX = x2-x1;
                float valueY = y2-y1;

                if(Math.abs(valueX)>MIN_DISTANCE){
                    if(x2>=x1 + MIN_DISTANCE){
                        //Toast.makeText(this, "Right Swipe", Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "Right Swipe log");
                        gameState.moveFallingTetraminoRight();
                    } else if (x2<=x1 - MIN_DISTANCE){
                        //Toast.makeText(this, "Left Swipe", Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "Left Swipe log");
                        gameState.moveFallingTetraminoLeft();
                    }
                } else if (Math.abs(valueY)>MIN_DISTANCE){
                    if (y2>=y1 + MIN_DISTANCE){
                        //Toast.makeText(this, "Down Swipe", Toast.LENGTH_SHORT).show();
                        if (!gameState.difficultMode) {
                            delay = delay / delayFactor;
                            gameState.difficultMode = true;
                            difficultyToggle.setText(R.string.hard);
                        } else {
                            delay = delay * delayFactor;
                            difficultyToggle.setText(R.string.easy);
                            gameState.difficultMode = false;

                        }
                        Log.d(TAG, "Down Swipe log");
                    } else if (y2<=y1 - MIN_DISTANCE){
                        //Toast.makeText(this, "Up Swipe", Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "Up Swipe log");
                        gameState.rotateFallingTetraminoAntiClock();
                    }
                }
        }
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        return false;
    }

    //_____________________________________________________________________________________________
//    public void restart(View view){
//        Intent intent3 = new Intent(MainActivity.this, MainActivity.class);
//        startActivity(intent3);
//        finish();
//    }

    @Override
    public void onClick(View action) {
        if (action == left) {
            gameState.moveFallingTetraminoLeft();

        } else if (action == right) {
            gameState.moveFallingTetraminoRight();

        } else if (action == rotateAc) {
            gameState.rotateFallingTetraminoAntiClock();

        } else if (action == pause) {
            if (gameState.status) {
                if (gameState.pause) {
                    gameState.pause = false;
                    pause.setText(R.string.pause);

                } else {
                    pause.setText(R.string.play);
                    gameState.pause = true;

                }
            } else {
                pause.setText(R.string.start_new_game);
                Toast.makeText(getApplicationContext(), "Play Again or Restart", Toast.LENGTH_SHORT).show();
//                ///RESTART THE GAME///
//                // Restart game when start_new_game is pressed
//                View startNewGameButtonListener = findViewById(R.id.pause);
//                startNewGameButtonListener.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        // Stop the game loop
////                        handler.removeCallbacks(loop);
////
////                        // Reset the game state
////                        gameState = new GameState(24, 20, TetraminoType.getRandomTetramino());
////                        //drawView.setGameState(gameState);
////
////                        //drawView = new DrawView(this, gameState);
////                        drawView.setBackgroundResource(R.drawable.img);
////
////                        // Reset the delay
////                        delay = 50;
////                        delayLowerLimit = 200;
////                        delayFactor = 2;
////
////                        // Reset the score
////                        gameState.score = 0;
////                        //score.setText(getString(R.string.score, 0);
////
////                        // Restart the game loop
////                        handler.postDelayed(loop, delay);
//                    }
//                });
//
            }
        } else if (action == difficultyToggle) {
            if (!gameState.difficultMode) {
                delay = delay / delayFactor;
                gameState.difficultMode = true;
                difficultyToggle.setText(R.string.hard);

            } else {
                delay = delay * delayFactor;
                difficultyToggle.setText(R.string.easy);
                gameState.difficultMode = false;

            }
        }
//        else if (action == restart){
//            Intent intent4 = new Intent(MainActivity.this, MainActivity.class);
//            startActivity(intent4);
//            finish();



//            ___________________________
//            handler.removeCallbacks(loop);
//
//            // Reset the game state
//            gameState = new GameState(24, 20, TetraminoType.getRandomTetramino());
//            //drawView.setGameState(gameState);
//
//            //drawView = new DrawView(this, gameState);
//            drawView.setBackgroundResource(R.drawable.img);
//
//            // Reset the delay
//            delay = 50;
//            delayLowerLimit = 200;
//            delayFactor = 2;
//
//            // Reset the score
//            gameState.score = 0;
//            //score.setText(getString(R.string.score, 0);
//
//            // Restart the game loop
//            handler.postDelayed(loop, delay);
        }
    }
