package com.example.aritetrisburg;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class GameOverScreen extends AppCompatActivity {
    Button toHomeButton, toLeaderboard, toRestart;
//    TextView first_place_points, second_place_points, third_place_points;
//    TextView tvHighest;
//    SharedPreferences sharedPreferences;
//    ImageView ivNewHighest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_over_screen);



        toHomeButton = (Button) findViewById(R.id.backButton);

        toHomeButton.setOnClickListener(v -> {
                    Intent intent1 = new Intent(GameOverScreen.this, HomeScreen.class);
                    startActivity(intent1);
                });


            toLeaderboard = (Button) findViewById(R.id.leaderboardButon);

            toLeaderboard.setOnClickListener(v -> {
                Intent intent2 = new Intent(GameOverScreen.this,ScoreLeaderboardScreen.class);
                startActivity(intent2);
        });

        toRestart = (Button) findViewById(R.id.restartButon);

        toRestart.setOnClickListener(v -> {
            Intent intent3 = new Intent(GameOverScreen.this,MainActivity.class);
            startActivity(intent3);
        });

    }
}